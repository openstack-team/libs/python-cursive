python-cursive (0.2.3-4) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090479).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 11:26:35 +0100

python-cursive (0.2.3-3) unstable; urgency=medium

  * Remove extraneous python3-six dependencies (Closes: #1068099).

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Apr 2024 13:51:15 +0200

python-cursive (0.2.3-2) unstable; urgency=medium

  * Cleans better (Closes: #1045732).

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Aug 2023 16:19:26 +0200

python-cursive (0.2.3-1) unstable; urgency=medium

  * New upstream release:
    - Fixes FTBFS because of unit tests (Closes: #1026702).
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Sun, 01 Jan 2023 15:44:04 +0100

python-cursive (0.2.2-4) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 12:33:24 +0200

python-cursive (0.2.2-3) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

  [ Thomas Goirand ]
  * Fixed upstream URLs in d/rules, d/copyright and d/control.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Apr 2020 17:20:23 +0200

python-cursive (0.2.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 16 Jul 2019 20:51:11 +0200

python-cursive (0.2.2-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Add trailing tilde to min version depend to allow
    backports
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Wed, 27 Mar 2019 10:18:52 +0100

python-cursive (0.2.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 22:56:29 +0000

python-cursive (0.2.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Feb 2018 07:30:00 +0000

python-cursive (0.1.2-3) unstable; urgency=medium

  * Team upload.
  * Have python3 package depend on python3:Depends (Closes: #865921)

 -- Ondřej Nový <onovy@debian.org>  Tue, 31 Oct 2017 14:45:42 +0100

python-cursive (0.1.2-2) unstable; urgency=medium

  * Uploading to unstable.
  * Standards-Version is now 4.1.1.

 -- Thomas Goirand <zigo@debian.org>  Sun, 29 Oct 2017 19:00:54 +0000

python-cursive (0.1.2-1) experimental; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 07 Oct 2017 11:05:58 +0200

python-cursive (0.1.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fixed oslotest EPOCH.
  * Added myself as uploader.
  * Using pkgos-dh_auto_{test,install} from openstack-pkg-tools >= 52~.

 -- Thomas Goirand <zigo@debian.org>  Wed, 28 Sep 2016 10:10:32 +0200

python-cursive (0.1.1-1) experimental; urgency=medium

  * Initial release (Closes: #836268).

 -- James Page <james.page@ubuntu.com>  Thu, 01 Sep 2016 09:24:31 +0100
