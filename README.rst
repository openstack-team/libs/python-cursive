===============================
cursive
===============================

Cursive implements OpenStack-specific validation of digital signatures.

As OpenStack continues to mature, robust security controls become increasingly
critical. The cursive project contains code extracted from various OpenStack
projects for verifying digital signatures. Additional capabilities will be
added to this project in support of various security features.

* Free software: Apache license
* Source: https://opendev.org/x/cursive
* Bugs: http://bugs.launchpad.net/cursive

Features
--------

* TODO
