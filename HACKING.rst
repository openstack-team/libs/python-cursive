cursive Style Commandments
===============================================

Read the OpenStack Style Commandments https://docs.openstack.org/hacking/latest/

cursive Specific Commandments
-----------------------------

- Copyright notices for organizations should not be included.
